import numpy as np
import lib.pyplotdefs as pd
import lib.peaks as peaks
import getopt
from pathlib import Path
import sys
import matplotlib.colors as mplcolors
from libscratch.pyplotdefs import S

hist_colors = mplcolors.LinearSegmentedColormap.from_list(
    "vibrant",
    [
        (0.000, "#012d5e"),
        (0.125, "#0039a7"),
        (0.250, "#1647cf"),
        (0.375, "#6646ff"),
        (0.500, "#bc27ff"),
        (0.600, "#dc47af"),
        (0.800, "#f57548"),
        (0.900, "#f19e00"),
        (0.950, "#fbb800"),
        (1.000, "#fec800"),
    ],
)

def process_hists():
    P_fid = pd.Plotter()
    P_photon = pd.Plotter()
    P_loading = pd.Plotter()
    P_threshold = pd.Plotter()
    bin_size = BIN_SIZE
    N_peaks = N_PEAKS
    skew_flag = SKEW_FLAG
    infile = INFOLDER.joinpath("roi_totals.npz")
    data = np.load(infile)
    sl = tuple([S[:] if n > 1 else 0 for n in data["roi_totals"].shape])
    # roi_totals = data["roi_totals"][:, :, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
    roi_totals = data["roi_totals"][sl]
    fidels = []
    atom_photon = []
    atom_photon_err = []
    loading_prob = []
    threshold = []
    for k, subroi in enumerate(roi_totals):
        subroi = subroi.flatten()
        analyzer = peaks.Analyzer(subroi, bin_size)
        analyzer.fit_peaks(N_peaks, skew_flag).find_thresholds()
        fidels.append(analyzer.quantities['derived_quantities']['fidelity'])
        threshold.append(analyzer.quantities['derived_quantities']['thresholds'][1])
        atom_photon.append(analyzer.quantities['fits'][1]['u'])
        atom_photon_err.append(analyzer.quantities['fits'][1]['s'])
        loading_prob.append(analyzer.quantities['fits'][1]['P'])
        analyzer.save(
            str(infile.parent.joinpath("quantities.toml")),
            str(infile.parent.joinpath(f"plot{k}.png")),
        )
        k1 = k + 1
        P_fid.plot(k, fidels[k], marker='o', linestyle="", color=hist_colors(k/(NT-1)))
        # P_photon.errorbar(k, atom_photon[k], atom_photon_err[k], marker='.', linestyle='', color=hist_colors(k/10))
        P_photon.errorbar(k, atom_photon[k], atom_photon_err[k], marker='o', linestyle='', color=hist_colors(k/(NT-1)))
        P_loading.plot(k, loading_prob[k], marker='o', linestyle='', color=hist_colors(k/(NT-1)))
        P_threshold.plot(k, threshold[k], marker='o', linestyle='', color=hist_colors(k/(NT-1)))
    P_fid.set_xlabel("tweezer_idx")
    P_fid.set_ylabel("fidelity")
    P_fid.savefig(str(infile.parent.joinpath("hist/fidelities.png")))
    P_photon.set_xlabel("tweezer_idx")
    P_photon.set_ylabel("average one atom photon scatter")
    P_photon.savefig(str(infile.parent.joinpath("hist/photon_scatter.png")))
    P_loading.set_xlabel("tweezer_idx")
    P_loading.set_ylabel("loading probability")
    P_loading.savefig(str(infile.parent.joinpath("fat/loading_prob.png")))
    P_threshold.set_xlabel("tweezer_idx")
    P_threshold.set_ylabel("backgroun/atom threshold")
    P_threshold.savefig(str(infile.parent.joinpath("hist/threshold.png")))
    return 0

def process_survival():
    infile = INFOLDER.joinpath("survival_data.npz")
    data = np.load(infile)
    surv = data['survival'].flatten()
    surv_err = data['survival_err'].flatten()
    nt = NT
    P = pd.Plotter()
    for i in range(nt):
        P.errorbar([i], surv[i], surv_err[i], marker='.', linestyle='', color=hist_colors((i)/(nt-1)))
        
    (P
        .set_ylabel("survival")
        .set_xlabel("tweezer_idx")
        # .set_ylim(0.7, 1.0)
        .savefig(INFOLDER.joinpath("survival/survival.png"))
        .close()
    )

def process_single():
    bin_size = BIN_SIZE
    N_peaks = N_PEAKS
    skew_flag = SKEW_FLAG
    infile = INFOLDER.joinpath("roi_totals.npz")
    data = np.load(infile)
    sl = tuple([S[:] if n > 1 else 0 for n in data["roi_totals"].shape])
    roi_totals = data["roi_totals"][sl].flatten()
    # roi_totals = data["roi_totals"][:, :, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0].flatten()
    analyzer = peaks.Analyzer(roi_totals, bin_size)
    analyzer.fit_peaks(N_peaks, skew_flag).find_thresholds()

    analyzer.save(
        str(infile.parent.joinpath("quantities.toml")),
        str(infile.parent.joinpath(f"hist/plot_avg.png")),
    )

def process_scan():
    pass


INFOLDER = (Path("C:/Users/Covey Lab/Documents/Andor Solis/atomic_data")
    .joinpath("20230705")
    .joinpath("hist_007")
)
BIN_SIZE: int = 3.0
N_PEAKS: int = 2
SKEW_FLAG: bool = False
NT = 5

if __name__ == "__main__":
    # process_survival()
    process_hists()
    process_single()

