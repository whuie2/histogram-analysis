import numpy as np
import lib.pyplotdefs as pd
import lib.peaks as peaks
import getopt
from pathlib import Path
import sys

INFILE: str = "data/2022.06.07.npy"
BIN_SIZE: int = 5
N_PEAKS: int = 2
SKEW_FLAG: bool = False

HELP_TEXT = """
Synopsis:
    Quick-and-dirty shell script to drive histogram analysis.

Usage:
    [python] hist_anl.py [ options... ] <data_file>
    [python] hist_anl.py --help|-h

Options:
    --bin-size | -b <int>
        Set the size of the photon counting bins.
    --N-peaks | -N <int>
        Set the number of peaks to fit to, including the zero-atom peak.
    --skew | -s
        Allow the fitting distributions to be skewed. The zero-atom peak is
        never skewed.
    --help | -h
        Display this text and exit.

Arguments:
    data_file
        Path to the data file to be read. Should be a .npy file whose content is
        a 1D array containing photon counts.
"""

class Config:
    def __init__(
        self,
        infile: str,
        bin_size: int,
        N_peaks: int,
        skew_flag: bool
    ):
        self.infile = str(infile)
        self.bin_size = int(bin_size)
        self.N_peaks = int(N_peaks)
        self.skew_flag = bool(skew_flag)

def main():
    config = Config(INFILE, BIN_SIZE, N_PEAKS, SKEW_FLAG)

    shortopts = "b:N:sh"
    longopts = [
        "bin-size=",
        "N-peaks=",
        "skew",
        "help",
    ]
    try:
        opts, args = getopt.gnu_getopt(sys.argv[1:], shortopts, longopts)
        for opt, optarg in opts:
            if opt in {"-b", "--bin-size"}:
                config.bin_size = int(optarg)
            elif opt in {"-N", "--N-peaks"}:
                config.N_peaks = int(optarg)
            elif opt in {"-s", "--skew"}:
                config.skew_flag = True
            elif opt in {"-h", "--help"}:
                print(HELP_TEXT)
                sys.exit(0)
        if len(args) > 0:
            config.infile = args[0]
    except Exception as ERR:
        raise ERR

    data = np.load(config.infile).flatten()
    analyzer = peaks.Analyzer(data, config.bin_size, config.infile)
    analyzer.fit_peaks(config.N_peaks, config.skew_flag).find_thresholds()
    infile = Path(config.infile)
    analyzer.save(
        str(infile.parent.joinpath(infile.stem + "_quantities.toml")),
        str(infile.parent.joinpath(infile.stem + "_plot.png")),
    )
    return 0

if __name__ == "__main__":
    sys.exit(main())

