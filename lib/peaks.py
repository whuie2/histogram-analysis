from __future__ import annotations
import numpy as np
from scipy.stats import norm, skewnorm
# from scipy.signal import find_peaks
import lib.pyplotdefs as pd
pd.pp.rcParams["font.size"] = 8.0
import lmfit
from typing import Union, TypeVar
NPReal = Union[float, np.float64, np.ndarray]
import toml

SKEW0: bool = False

class Peaks:
    """
    Holds parameters for the N-atom distributions and provides PDF/CDF methods
    for both the individual and the total distribution.

    Fields
    ------
    N_peaks : int
        Number of peaks, including the 0-atom peak.
    A, a, u, s, P, mo : list[float]
        Distribution parameters:
            A = amplitude
            a = skew
            u = mean
            s = std. dev.
            P = total probability = A sqrt(2 pi s^2)
            mo = mode
    Nbar : float
        Average number of atoms.
    skew_flag : bool
        Whether the distributions are allowed to be skewed.
    """

    N_peaks: int
    A: list[float]
    a: list[float]
    u: list[float]
    s: list[float]
    P: list[float]
    mo: list[float]
    Nbar: float
    skew_flag: bool

    def __init__(
        self,
        peak_params: list[(float, float, float, float)],
        skew_flag: bool,
    ):
        """
        Parameters
        ----------
        peak_params : list[ (float, float, float, float) ]
            Fit parameters for each of the individual distributions, where each
            element is a tuple arranged as
                (amplitude, skew, mean, std. dev.)
        skew_flag : bool
            Whether the distributions are allowed to be skewed.
        """
        self.N_peaks = len(peak_params)
        self.A, self.a, self.u, self.s = [list(x) for x in zip(*peak_params)]
        self.P = [
            ( Ak * np.sqrt(2.0 * np.pi * sk**2) / 2.0 )
            if skew_flag or (SKEW0 and k == 0) else ( Ak * np.sqrt(2.0 * np.pi * sk**2) )
            for k, (Ak, sk) in enumerate(zip(self.A, self.s))
        ]
        self.mo = [ 
            uk + sk * _sknorm_mo(ak, (SKEW0 and k == 0) or skew_flag)
            for k, (ak, uk, sk) in enumerate(zip(self.a, self.u, self.s))
        ]
        self.Nbar = sum(k * Pk for k, Pk in enumerate(self.P))
        self.skew_flag = skew_flag

    def pdf(self, k: int, x: NPReal) -> NPReal:
        """
        Non-normalized `k`-atom component of the total PDF.
        """
        return self.P[k] * (
            skewnorm.pdf(x, self.a[k], loc=self.u[k], scale=self.s[k])
            if self.skew_flag or (SKEW0 and k == 0) else norm.pdf(x, loc=self.u[k], scale=self.s[k])
        )

    def pdf_norm(self, k: int, x: NPReal) -> NPReal:
        """
        PDF for just the `k`-atom distribution.
        """
        return (
            skewnorm.pdf(x, self.a[k], loc=self.u[k], scale=self.s[k])
            if self.skew_flag or (SKEW0 and k == 0) else norm.pdf(x, loc=self.u[k], scale=self.s[k])
        )

    def pdf_tot(self, x: NPReal) -> NPReal:
        """
        PDF for the total distribution.
        """
        return sum(self.pdf(k, x) for k in range(self.N_peaks))

    def cdf(self, k: int, x: NPReal) -> NPReal:
        """
        Non-normalized `k`-atom component of the total CDF.
        """
        return self.P[k] * (
            skewnorm.cdf(x, self.a[k], loc=self.u[k], scale=self.s[k])
            if self.skew_flag or (SKEW0 and k == 0) else norm.cdf(x, loc=self.u[k], scale=self.s[k])
        )

    def cdf_norm(self, k: int, x: NPReal) -> NPReal:
        """
        CDF for just the `k`-atom distribution.
        """
        return (
            skewnorm.cdf(x, self.a[k], loc=self.u[k], scale=self.s[k])
            if self.skew_flag or (SKEW0 and k == 0) else norm.cdf(x, loc=self.u[k], scale=self.s[k])
        )

    def cdf_tot(self, x: NPReal) -> NPReal:
        """
        CDF for the total distribution.
        """
        return sum(self.cdf(k, x) for k in range(self.N_peaks))

    def tcr(self, k: int, Xk: float) -> float:
        """
        True classification rate for judgements of the form
            x <  Xk ===> there are fewer than k atoms
            x >= Xk ===> there are at least k atoms
        """
        return (
            sum(self.cdf(i, Xk) for i in range(k))
            if k < self.N_peaks else 0.0
        ) - sum(self.cdf(j, Xk) for j in range(k, self.N_peaks))

    def tcr_exact(self, k: int, Xk: float, Xkp1: float) -> float:
        """
        Non-normalized true classification rate for judgements of the form
            Xk <= x < Xkp1 ===> there are exactly k atoms
        """
        return self.cdf(k, Xkp1) - self.cdf(k, Xk)

    def tcr_exact_norm(self, k: int, Xk: float, Xkp1: float) -> float:
        """
        True classification rate for judgements of the form
            Xk <= x < Xkp1 ===> there are exactly k atoms
        """
        return self.cdf_norm(k, Xkp1) - self.cdf_norm(k, Xk)

    def tcr_tot(self, X: list[float]) -> float:
        """
        Overall true classification rate for judgements of the form
            x <  X[k] ===> there are fewer than k atoms
            x >= X[k] ===> there are at least k atoms
        Usually requires that X[0] == -inf for a reasonable result and assumes
        X[N_peaks] == +inf.
        """
        return sum(self.tcr(k, Xk) for k, Xk in enumerate(X))

    def fcr(self, k: int, xk: float) -> float:
        """
        False classification rate for judgements of the form
            x <  Xk ===> there are fewer than k atoms
            x >= Xk ===> there are at least k atoms
        """
        return (
            (
                sum(self.P[k] - self.cdf(i, xk) for i in range(k))
                if k < self.N_peaks else 0.0
            )
            + sum(self.cdf(j, xk) for j in range(k, self.N_peaks))
        )

    def fcr_exact(self, k: int, Xk: float, Xkp1: float) -> float:
        """
        Non-normalized false classification rate for judgements of the form
            Xk <= x < Xkp1 ===> there are exactly k atoms
        """
        return P[k] - self.cdf(k, Xkp1) + self.cdf(k, Xk)

    def fcr_exact_norm(self, k: int, Xk: float, Xkp1: float) -> float:
        """
        False classification rate for judgements of the form
            Xk <= x < Xkp1 ===> there are exactly k atoms
        """
        return 1.0 - self.cdf_norm(k, Xkp1) + self.cdf_norm(k, Xk)

    def fcr_tot(self, X: list[float]) -> float:
        """
        Overall false classification rate for judgements of the form
            x <  X[k] ===> there are fewer than k atoms
            x >= X[k] ===> there are at least k atoms
        Usually requires that X[0] == -inf for a reasonable result and assumes
        X[N_peaks] == +inf.
        """
        return sum(self.fcr(k, Xk) for k, Xk in enumerate(X))

    def fidelity(self, X: list[float]) -> float:
        """
        Overall detection fidelity.
        """
        return sum(
            self.tcr_exact(k, Xk, Xkp1)
            for k, (Xk, Xkp1) in enumerate(zip(X[:-1], X[1:]))
        )

    def fidelity_0n(self, X: list[float]) -> float:
        """
        Overall zero-or-n detection fidelity.
        """
        return (
            self.cdf(0, X[1])
            + sum(
                self.P[k] - self.cdf(k, X[1])
                for k in range(1, self.N_peaks)
            )
        )

    def fidelities(self, X: list[float]) -> list[float]:
        """
        Normalized per-peak fidelities.
        """
        return [
            self.tcr_exact_norm(k, Xk, Xkp1)
            for k, (Xk, Xkp1) in enumerate(zip(X[:-1], X[1:]))
        ]

    def infidelity(self, X: list[float]) -> float:
        """
        Overall detection infidelity.
        """
        return sum(
            self.fcr_exact(k, Xk, Xkp1)
            for k, (Xk, Xkp1) in enumerate(zip(X[:-1], X[1:]))
        )

    def infidelity_0n(self, X: list[float]) -> float:
        """
        Overall zero-or-n detection infidelity.
        """
        return 1.0 - self.fidelity_0n(X)

    def infidelities(self, X: list[float]) -> list[float]:
        """
        Normalized per-peak infidelities.
        """
        return [
            self.fcr_exact_norm(k, Xk, Xkp1)
            for k, (Xk, Xkp1) in enumerate(zip(X[:-1], X[1:]))
        ]

def gen_hist(
    data: np.ndarray,
    bin_size: int,
) -> (np.ndarray, np.ndarray, pd.Plotter):
    """
    Generate a histogram from photon counts.

    Parameters
    ----------
    data : numpy.ndarray[ndim=1, dtype=numpy.float64]
        Total photon counts within a ROI.
    bin_size : int
        Histogram bin size.

    Returns
    -------
    p : numpy.ndarray[ndim=1, dtype=numpy.float64]
        Probability density.
    x : numpy.ndarray[ndim=1, dtype=numpy.float64]
        Bin centers.
    plt : pyplotdefs.Plotter
        Plotter object holding the generated histogram.
    """
    plt = pd.Plotter()
    p, bins, _ = plt.hist(
        data,
        bins=np.arange(data.min() - 0.5, data.max() + bin_size + 0.5, bin_size),
        density=True,
        edgecolor="k",
        linewidth=0.5,
        chain=False,
    )
    x = (bins[:-1] + bins[+1:]) / 2.0
    (plt
        .set_xlabel("Photons")
        .set_ylabel("Probability density")
        .ggrid().grid(False, which="both")
    )
    return p, x, plt

def _sknorm_delta(a: float) -> float:
    return a / np.sqrt(1.0 + a**2.0)

def _sknorm_skewness(a: float) -> float:
    return (
        (4.0 - np.pi) / 2.0
        * (_sknorm_delta(a) * np.sqrt(2.0 / np.pi))**3.0
        / (1.0 - 2.0 * _sknorm_delta(a)**2.0 / np.pi)**(3.0 / 2.0)
    )

def _sknorm_uz(a: float) -> float:
    return np.sqrt(2.0 / np.pi) * _sknorm_delta(a)

def _sknorm_sz(a: float) -> float:
    return np.sqrt(1.0 - _sknorm_uz(a)**2.0)

def _sknorm_mo(a: float, skew_flag: bool) -> float:
    return (
        _sknorm_uz(a)
        - _sknorm_skewness(a) * _sknorm_sz(a) / 2.0
        - a / abs(a) / 2.0 * np.exp(-2.0 * np.pi / abs(a))
    ) if a != 0.0 and skew_flag else 0.0

def _multi_gaussian(
    params: lmfit.Parameters,
    x: NPReal,
    skew_flag: bool,
    N_peaks: int,
) -> NPReal:
    A = [params[f"A{k}"].value for k in range(N_peaks)] # amplitude
    a = [params[f"a{k}"].value for k in range(N_peaks)] # skew
    u = [params[f"u{k}"].value for k in range(N_peaks)] # mean
    s = [params[f"s{k}"].value for k in range(N_peaks)] # std. dev.
    P = [
        ( Ak * np.sqrt(2.0 * np.pi * sk**2) / 2.0 )
        if skew_flag or (SKEW0 and k == 0) else ( Ak * np.sqrt(2.0 * np.pi * sk**2) )
        for k, (Ak, sk) in enumerate(zip(A, s))
    ]
    return sum(
        Pk * (
            skewnorm.pdf(x, ak, loc=uk, scale=sk)
            if skew_flag or (SKEW0 and k == 0) else norm.pdf(x, loc=uk, scale=sk)
        )
        for k, (Pk, ak, uk, sk) in enumerate(zip(P, a, u, s))
    )

def _residuals(
    params: lmfit.Parameters,
    x: NPReal,
    p: NPReal,
    skew_flag: bool,
    N_peaks: int,
) -> NPReal:
    m = _multi_gaussian(params, x, skew_flag, N_peaks)
    return (m - p)**2

def fit_gaussians(
    x: np.ndarray,
    p: np.ndarray,
    N_peaks: int,
    skew_flag: bool,
) -> Peaks:
    """
    Fit the given distribution to multiple Gaussians.

    Parameters
    ----------
    x : numpy.ndarray[ndim=1, dtype=numpy.float64]
        Bin centers.
    p : numpy.ndarray[ndim=1, dtype=numpy.float64]
        Probability densities.
    N_peaks : int
        Number of Gaussians, including the zero-atom peak.
    skew_flag : bool
        Whether the Gaussians are allowed to be skewed. The zero-atom peak is
        always skewed.

    Returns
    -------
    peaks : Peaks
        Holds parameters from the fit and provides methods to compute
        distribution functions and classification fidelities.
    """
    Ptot = 1.0
    s_init = [3.0, x.max() / N_peaks / 3.0]
    while True:
        try:
            fitparams = _fit_gaussians(x, p, N_peaks, skew_flag, Ptot, s_init)
        except Exception as err:
            print(err)
            pd.Plotter().plot(x, p).show().close()
            Ptot = np.random.normal(loc=1.0, scale=0.25)
            s_init = [3.0, x.max() / N_peaks / np.random.normal(loc=2.0, scale=1.0)]
            continue
        if all(p[0] > 0.0 for p in fitparams):
            break
        else:
            print("fit stall")
            pd.Plotter().plot(x, p).show().close()
            Ptot = np.random.normal(loc=0.0, scale=0.25)
            s_init = [3.0, x.max() / N_peaks / np.random.normal(loc=2.0, scale=1.0)]
            continue
    return Peaks(fitparams, skew_flag)

def _fit_gaussians(
    x: np.ndarray,
    p: np.ndarray,
    N_peaks: int,
    skew_flag: bool,
    Ptot: float,
    s_init: list[float, 2],
) -> Peaks:
    params = lmfit.Parameters()
    for k in range(N_peaks):
        is_background = 1 if k > 0 else 0
        A_init = (Ptot / N_peaks) / np.sqrt(2.0 * np.pi) / s_init[is_background]
        u_init = (k * s_init[1]) if k > 0 else 3.0
        params.add(f"a{k}", value=0.0, vary=True if (SKEW0 and k == 0) else skew_flag)
        params.add(f"s{k}", value=s_init[is_background], min=0.0, max=x.max())
        params.add(f"u{k}", value=u_init, min=0.0, max=x.max())
        if k < N_peaks - 1:
            params.add(f"A{k}", value=A_init, min=0.0)
        else:
            params.add(f"A{k}",
                expr=(
                    "(1 - "
                    + " - ".join(
                        f"A{j} * sqrt(2 * pi * s{j}**2)"
                        + f" / {'2' if skew_flag or (SKEW0 and j == 0) else '1'}"
                        for j in range(N_peaks - 1)
                    )
                    + f") * {'2' if skew_flag or (SKEW0 and k == 0) else '1'} / sqrt(2 * pi * s{k}**2)"
                )
            )
    fit = lmfit.minimize(_residuals, params, args=(x, p, skew_flag, N_peaks))
    if not fit.success:
        raise Exception("Error fitting distribution")

    fitparams = [
        (
            fit.params[f"A{k}"].value,
            fit.params[f"a{k}"].value,
            fit.params[f"u{k}"].value,
            fit.params[f"s{k}"].value,
        )
        for k in range(N_peaks)
    ]
    fitparams.sort(key=lambda p: p[2])
    return fitparams

def _threshold_costf(params: lmfit.Parameters, peaks: Peaks) -> float:
    # X[k] represent cutoffs for judgements like:
    #   | x <  X[k] ===> there are fewer than k atoms
    #   | x >= X[k] ===> there are at least k atoms
    X = [params[f"x{k}"].value for k in range(peaks.N_peaks + 1)]
    return peaks.fcr_tot(X)

def find_thresholds(peaks: Peaks) -> list[float]:
    """
    Find optimal locations of thresholds.

    Parameters
    ----------
    peaks : Peaks
        Photon counting distribution.

    Returns
    -------
    X : list[float, len=peaks.N_peaks + 1]
        Locations of thresholds: The k-th element of this list represents the
        cutoff for judgements like:
            x <  X[k] ===> there are fewer than k atoms
            x >= X[k] ===> there are at least k atoms
        X[0] is always -numpy.inf, and X[peaks.N_peaks] is always +numpy.inf.
    """
    params = lmfit.Parameters()
    params.add("x0", value=-np.inf, vary=False)
    for k in range(1, peaks.N_peaks):
        params.add(f"x{k}",
            value=(peaks.u[k] + peaks.u[k - 1]) / 2.0,
            min=peaks.u[k - 1],
            max=peaks.u[k],
        )
    params.add(f"x{peaks.N_peaks}", value=np.inf, vary=False)
    fit = lmfit.minimize(
        _threshold_costf,
        params,
        args=(peaks,),
        method="differential_evolution"
    )
    if not fit.success:
        raise Exception("Error setting thresholds")
    X = [fit.params[f"x{k}"].value for k in range(peaks.N_peaks + 1)]
    X.sort()
    return X

Self = TypeVar("Self")
class Analyzer:
    """
    Driver class for the whole process of fitting s distribution to data,
    finding optimal thresholds, and calculating total detection fidelity.
    """
    def __init__(self, data: np.ndarray, bin_size: int, infile: str=None):
        self.infile = infile
        self.data = data
        self.p, self.x, self.plt = gen_hist(data, bin_size)
        self.peaks = None
        self.thresholds = None
        self.quantities = {
            "fits": list(),
            "parameters": {
                "infile": str(infile),
                "bin_size": int(bin_size),
            },
            "derived_quantities": dict(),
        }

    def fit_peaks(self, N_peaks: int, skew_flag: bool) -> Self:
        self.peaks = fit_gaussians(self.x, self.p, N_peaks, skew_flag)
        self.quantities["parameters"]["N_peaks"] = int(N_peaks)
        self.quantities["fits"] = [
            {
                "A": float(Ak),
                "a": float(ak),
                "u": float(uk),
                "s": float(sk),
                "P": float(Pk),
                "mo": float(mok),
            }
            for Ak, ak, uk, sk, Pk, mok in zip(
                self.peaks.A,
                self.peaks.a,
                self.peaks.u,
                self.peaks.s,
                self.peaks.P,
                self.peaks.mo,
            )
        ]
        self.quantities["derived_quantities"]["Nbar"] = float(self.peaks.Nbar)
        xplot = np.linspace(
            self.x.min() - (self.x.max() - self.x.min()) / 20.0,
            self.x.max() + (self.x.max() - self.x.min()) / 20.0,
            1000
        )
        ylim = self.plt.get_ylim()
        for k, (uk, sk, Pk, mok) \
        in enumerate(zip(
            self.peaks.u,
            self.peaks.s,
            self.peaks.P,
            self.peaks.mo,
        )):
            (self.plt
                .plot(
                    xplot, self.peaks.pdf(k, xplot),
                    color="k", linestyle="--"
                )
                .text(
                    mok, self.peaks.pdf(k, mok) + 0.015 * (ylim[1] - ylim[0]),
                    f"{k} atom{'s' if k != 1 else '':s}"
                    + f"\n{uk:.1f} $\\pm$ {sk:.1f}",
                    fontsize="x-small",
                    ha="center", va="bottom",
                    bbox=dict(
                        alpha=0.75,
                        pad=0.2,
                        facecolor="w",
                        linewidth=0.0,
                    ),
                )
                .text(
                    mok, self.peaks.pdf(k, mok) / 2.0,
                    f"{100.0 * Pk:.1f}%",
                    fontsize="x-small",
                    ha="center", va="center",
                    bbox=dict(
                        alpha=0.75,
                        pad=0.2,
                        facecolor="w",
                        linewidth=0.0,
                    ),
                )
            )
        self.plt.plot(
            xplot, self.peaks.pdf_tot(xplot),
            color="k", linestyle="-"
        )
        return self

    def find_thresholds(self) -> Self:
        self.thresholds = find_thresholds(self.peaks)
        self.quantities["derived_quantities"]["thresholds"] \
            = [float(Xk) for Xk in self.thresholds]
        self.quantities["derived_quantities"]["true_rates"] \
            = [
                float(self.peaks.tcr_exact_norm(k, Xk, Xkp1))
                for k, (Xk, Xkp1)
                in enumerate(
                    zip(self.thresholds[:-1], self.thresholds[1:])
                )
            ]
        self.quantities["derived_quantities"]["false_rates"] \
            = [
                float(self.peaks.fcr_exact_norm(k, Xk, Xkp1))
                for k, (Xk, Xkp1)
                in enumerate(
                    zip(self.thresholds[:-1], self.thresholds[1:])
                )
            ]
        self.quantities["derived_quantities"]["fidelity"] \
            = float(self.peaks.fidelity(self.thresholds))
        ylim = self.plt.get_ylim()
        for Xk in self.thresholds[1:-1]:
            (self.plt
                .axvline(Xk, color="gray", linestyle=":")
                .text(
                    Xk, 0.015 * (ylim[1] - ylim[0]),
                    f"{Xk:.1f}",
                    fontsize="x-small",
                    ha="center", va="bottom",
                    bbox=dict(
                        alpha=0.75,
                        pad=0.2,
                        facecolor="w",
                        linewidth=0.0,
                    ),
                )
            )
        self.plt.set_title(
            f"Fidelity = {100.0 * self.quantities['derived_quantities']['fidelity']:.3f}%"
        )
        return self

    def report(self) -> dict[[str, ...] | dict[str, ...]]:
        return self.quantities

    def save(self, outfile: str, plot: str=None) -> Self:
        with open(outfile, 'w') as out:
            toml.dump(self.quantities, out)
        if plot is not None:
            self.plt.savefig(plot)
        return self

