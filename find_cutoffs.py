import numpy as np
from scipy.stats import norm, skewnorm
import lib.pyplotdefs as pd
pd.pp.rcParams["font.size"] = 8.0
# pd.set_font("/usr/share/fonts/OTF/MyriadPro-Regular.otf", "MyriadPro")
import lmfit
import toml
from typing import Union
NPReal = Union[float, np.float64, np.ndarray]
import sys

infile: str = "data/2022.06.07.npy"
bin_size: int = 5
Nmax: int = 1 # maximum number of atoms expected in the tweezer
skew_flag: bool = False # allow skewed Gaussians for atoms > 0

roi_totals: np.ndarray = np.load(infile).flatten()
# roi_totals: np.ndarray = np.load(infile)["roi_totals"][0, :, 0, 0, 3]
N_peaks: int = Nmax + 1

### FIT TO HISTOGRAM

PLT = pd.Plotter()
n, bins, _ = PLT.hist(
    roi_totals,
    bins=np.arange(
        -5 * bin_size,
        roi_totals.max() + 2.5 * bin_size,
        bin_size
    ),
    density=True,
    edgecolor="k",
    linewidth=0.5,
    chain=False
)
x = (bins[:-1] + bins[+1:]) / 2.0

def multi_gaussian(params: lmfit.Parameters, x: NPReal, skew: bool) -> NPReal:
    A = [params[f"A{k}"].value for k in range(N_peaks)]
    a = [params[f"a{k}"].value for k in range(N_peaks)]
    u = [params[f"u{k}"].value for k in range(N_peaks)]
    s = [params[f"s{k}"].value for k in range(N_peaks)]
    P = [
        ( Ak * np.sqrt(2.0 * np.pi * sk**2) / 2.0 )
        if skew else ( Ak * np.sqrt(2.0 * np.pi * sk**2) )
        for Ak, sk in zip(A, s)
    ]
    return sum(
        Pk * (
            skewnorm.pdf(x, ak, loc=uk, scale=sk)
            if skew else norm.pdf(x, loc=uk, scale=sk)
        )
        for Pk, ak, uk, sk in zip(P, a, u, s)
    )

def residuals(
    params: lmfit.Parameters,
    x: NPReal,
    p: NPReal,
    skew: bool,
) -> NPReal:
    m = multi_gaussian(params, x, skew)
    return (m - p)**2

params = lmfit.Parameters()
for k in range(N_peaks):
    s_init = x.max() / (N_peaks + 1) / 1.2
    u_init = (k + 1) * s_init
    A_init = (1 / N_peaks) / np.sqrt(2.0 * np.pi) / s_init

    params.add(f"a{k}", value=0.0, vary=False if k == 0 else skew_flag)
    params.add(f"s{k}", value=s_init, min=0.0, max=x.max())
    params.add(f"u{k}", value=u_init, min=0.0, max=x.max())
    if k < N_peaks - 1:
        params.add(f"A{k}", value=A_init, min=0.0)
    else:
        params.add(f"A{k}",
            expr=(
                "(1 - "
                + " - ".join(
                    f"A{j} * sqrt(2 * pi * s{j}**2)"
                    + f" / {'2' if skew_flag else '1'}"
                    for j in range(N_peaks - 1)
                )
                + f") * {'2' if skew_flag else '1'} / sqrt(2 * pi * s{k}**2)"
            )
        )
fit = lmfit.minimize(residuals, params, args=(x, n, skew_flag))
if not fit.success:
    print("Error fitting distributions")
    sys.exit(1)

fitparams = [
    (
        fit.params[f"A{k}"].value,
        fit.params[f"a{k}"].value,
        fit.params[f"u{k}"].value,
        fit.params[f"s{k}"].value,
    )
    for k in range(N_peaks)
]
fitparams.sort(key=lambda p: p[2])
A, a, u, s = [list(x) for x in zip(*fitparams)]
P = [
    ( Ak * np.sqrt(2.0 * np.pi * sk**2) / 2.0 )
    if skew_flag else ( Ak * np.sqrt(2.0 * np.pi * sk**2) )
    for Ak, sk in zip(A, s)
]

pdf = lambda k, x: (
    P[k] * (
        skewnorm.pdf(x, a[k], loc=u[k], scale=s[k])
        if skew_flag else norm.pdf(x, loc=u[k], scale=s[k])
    )
)
pdf_tot = lambda x: sum(pdf(k, x) for k in range(N_peaks))

cdf = lambda k, x: (
    P[k] * (
        skewnorm.cdf(x, a[k], loc=u[k], scale=s[k])
        if skew_flag else norm.cdf(x, loc=u[k], scale=s[k])
    )
)
cdf_tot = lambda x: sum(cdf(k, x) for k in range(N_peaks))

fcr = lambda k, xk: (
    (sum(P[k] - cdf(i, xk) for i in range(k)) if k < N_peaks else 0.0)
    + sum(cdf(j, xk) for j in range(k, N_peaks))
)

sknorm_delta = lambda a: a / np.sqrt(1 + a**2)
sknorm_skewness = lambda a: (
    (4 - np.pi) / 2
    * (sknorm_delta(a) * np.sqrt(2 / np.pi))**3
    / (1 - 2 * sknorm_delta(a)**2 / np.pi)**(3 / 2)
)
sknorm_uz = lambda a: np.sqrt(2 / np.pi) * sknorm_delta(a)
sknorm_sz = lambda a: np.sqrt(1 - sknorm_uz(a)**2)
sknorm_mo = lambda a: (
    (
        sknorm_uz(a)
        - sknorm_skewness(a) * sknorm_sz(a) / 2
        - (
            a / abs(a) / 2 * np.exp(-2 * np.pi / abs(a))
        )
    ) if a != 0 and skew_flag else 0.0
)

xplot = np.linspace(
    x.min() - (x.max() - x.min()) / 20.0,
    x.max() + (x.max() - x.min()) / 20.0,
    1000
)
ylim = PLT.get_ylim()
for k in range(N_peaks):
    _x_ = u[k] + s[k] * sknorm_mo(a[k])
    (PLT
        .plot(xplot, pdf(k, xplot), color="k", linestyle="--")
        .text(
            _x_, pdf(k, _x_) + 0.015 * (ylim[1] - ylim[0]),
            f"{k} atom{'s' if k != 1 else '':s}\n{u[k]:.1f} $\\pm$ {s[k]:.1f}",
            fontsize="x-small",
            ha="center", va="bottom",
            bbox=dict(
                alpha=0.75,
                pad=0.2,
                facecolor="w",
                linewidth=0.0,
            ),
        )
        .text(
            _x_, pdf(k, _x_) / 2.0,
            f"{100.0 * P[k]:.1f}%",
            fontsize="x-small",
            ha="center", va="center",
            bbox=dict(
                alpha=0.75,
                pad=0.2,
                facecolor="w",
                linewidth=0.0,
            ),
        )
    )
PLT.plot(xplot, pdf_tot(xplot), color="k", linestyle="-")
yvals = PLT.get_yticks()
(PLT
    # .set_yticklabels([f"{bin_size * y:.1f}" for y in yvals])
    .set_xlabel("Photons")
    .set_ylabel("Probability density")
    .ggrid().grid(False, which="both")
    .savefig("loading_fractions.png")
)

### FIND OPTIMAL CUTOFFS

def thresh_costf(params: lmfit.Parameters) -> float:
    # X[k] represent cutoffs for judgements like:
    #   | x <  X[k] ===> there are less than k atoms
    #   | x >= X[k] ===> there are at least k atoms
    X = [params[f"x{k}"].value for k in range(N_peaks + 1)]
    return sum(fcr(k, X[k]) for k in range(N_peaks + 1))

params = lmfit.Parameters()
params.add("x0", value=-np.inf, vary=False)
for k in range(1, N_peaks):
    params.add(f"x{k}", value=(u[k] + u[k - 1]) / 2.0, min=u[k - 1], max=u[k])
params.add(f"x{N_peaks}", value=np.inf, vary=False)
fit = lmfit.minimize(thresh_costf, params, method="differential_evolution")
if not fit.success:
    print("Error setting thresholds")
    sys.exit(1)

X = [fit.params[f"x{k}"].value for k in range(N_peaks + 1)]
for xk in X[1:-1]:
    (PLT
        .axvline(xk, color="gray", linestyle=":")
        .text(
            xk, 0.015 * (ylim[1] - ylim[0]),
            f"{xk:.1f}",
            fontsize="x-small",
            ha="center", va="bottom",
            bbox=dict(
                alpha=0.75,
                pad=0.2,
                facecolor="w",
                linewidth=0.0,
            ),
        )
    )

### CALCULATE TRUE/FALSE CLASSIFICATION RATES

# rates are for judgements like:
#   | X[k] <= x < X[k] ===> there are exactly k atoms
tr = [ # true rate
    (cdf(k, X[k + 1]) - cdf(k, X[k])) / P[k]
    for k in range(N_peaks)
]
fr = [ # false rate
    (P[k] - cdf(k, X[k + 1]) + cdf(k, X[k])) / P[k] for k in range(N_peaks)]
F = sum(trk * Pk for trk, Pk in zip(tr, P))
nbar = sum(k * P[k] for k in range(N_peaks))

F01 = (
    cdf(0, X[1])
    + sum(P[k] - cdf(k, X[1]) for k in range(1, N_peaks))
)

print(
    "\n".join(
        f"True/False {k} rate:"
        f" {100.0 * tr[k]:.1f}% / {100.0 * fr[k]:.1f}%"
        f" ({100.0 * P[k]:.1f}%)"
        for k in range(N_peaks)
    )
    + f"\nFidelity: {100.0 * F:.1f}%"
    + f"\n0-or-1 Fidelity: {100.0 * F01:.1f}%"
    + f"\nnbar: {nbar:.2f}"
)

(PLT
    .set_title(
        f"N-atom classification fidelity = {100.0 * F:.1f}%"
        + f"\n$\\geq 1$-atom classification fidelity = {100.0 * F01:.1f}%"
    )
    .savefig("hist_cutoffs.png")
)

with open("quantities.toml", 'w') as outfile:
    toml.dump(
        {
            "parameters": {
                "infile": infile,
                "N_peaks": int(N_peaks),
                "bin_size": float(bin_size),
            },
            "fits": [
                {
                    "A": float(A[k]),
                    "a": float(a[k]),
                    "u": float(u[k]),
                    "s": float(s[k]),
                    "P": float(P[k]),
                }
                for k in range(N_peaks)
            ],
            "derived_quantities": {
                "cutoffs": [float(Xk) for Xk in X],
                "true_rates": [float(trk) for trk in tr],
                "false_rates": [float(frk) for frk in fr],
                "fidelity": float(F),
                "nbar": float(nbar),
            },
        },
        outfile
    )


